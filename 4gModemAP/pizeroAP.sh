#!/bin/bash

# Peter Burke 11/2/2018
# Based off of: https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
# and
# https://github.com/Phoenix1747/RouteryPi/blob/master/install.sh
#
# This creates a wireless access point on WLAN0 for local networking only.
# It assumes eth0 is for internet access and already configured.
# For the application this was written for, the machine is Raspberry Pi Zero W.
# The eth0 is a USB 4g modem from Verizon, model USB730L.
# There is no communication between the two. 
# The above tutorial does to bridge the interfaces 
# does not function for this 4g modem for some reason.
#
# Therefore, all the "bridge" options are commented out, until someone can figure out how to fix.
# For now, you get an access point on wlan0 and internet access on eth0.
# 
# To save money on the 4g modem data fees, the wlan0 is used to download all the packages.
# So make sure the wlan0 is connected to the internet before running this.

# 11/15/2021 revisiting, found this website from Phoenix1747:
# Info: This tutorial is likely outdated. For an up-to-date version of how to set up an AP visit the official Raspberry Pi Documentation:
# Setting up a Routed Wireless Access Point 
# https://www.raspberrypi.org/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point
# Setting up a Bridged Wireless Access Point
# https://www.raspberrypi.org/documentation/computers/configuration.html#setting-up-a-bridged-wireless-access-point


# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will transform your Pi into a WiFi Access Point."
read -p "To begin with the installation type in 'yes': " out

if ! [ "$out" = "yes" ]
then
  echo "You did not type in 'yes'. Exiting..."
  exit 1
fi

read -p "Please specify your SSID: " my_ssid
read -p "Please specify a password: " my_pw

if [ "$my_ssid" ] && [ "$my_pw" ]
then
    echo "SSID and password successfully entered, continuing..."
else
  echo "Empty SSID, password or country code! Exiting..."
  exit 1
fi

echo "Starting..."

date


start_time="$(date -u +%s)"


# *** needs to be brought back up at end of script
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Starting"

echo "Updating package sources..."
sudo apt-get update -y
sudo apt-get upgrade -y
check_errors
echo "Done updating package sources..."

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Installing hostapd dnsmasq emacs bridge-utils..."
sudo apt-get install hostapd -y
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo apt-get install dnsmasq -y
sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent
sudo rfkill unblock wlan



echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Working on /etc/dhcpcd.conf ..."
echo "Appending to /etc/dhcpcd.conf ..."
a="
interface wlan0
static ip_address=192.168.4.1/24
nohook wpa_supplicant
"
sudo sh -c "echo '$a'>>/etc/dhcpcd.conf"
sudo chmod 777 /etc/dhcpcd.conf
check_errors
echo "Appended to /etc/dhcpcd.conf"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"



echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Working on /etc/dnsmasq.conf..."
# backup old file
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
echo "Writing to /etc/dnsmasq.conf ..."
a="
interface=wlan0 # Listening interface
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
                # Pool of IP addresses served via DHCP
domain=wlan     # Local wireless DNS domain
address=/gw.wlan/192.168.4.1
                # Alias for this router
"
sudo sh -c "echo '$a'>/etc/dnsmasq.conf"
sudo chmod 777 /etc/dnsmasq.conf
check_errors
echo "Wrote to /etc/dnsmasq.conf"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Working on /etc/hostapd/hostapd.conf..."
echo "Configuring hostapd"
echo "Writing to /etc/hostapd/hostapd.conf ..."
a="
country_code=US
interface=wlan0
ssid=SSIDCHANGEME
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=PASSPHRASECHANGEME
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
"

sudo sh -c "echo '$a'>/etc/hostapd/hostapd.conf"

sed -i "s/SSIDCHANGEME/$my_ssid/g" /etc/hostapd/hostapd.conf
sed -i "s/PASSPHRASECHANGEME/$my_pw/g" /etc/hostapd/hostapd.conf


sudo chmod 777 /etc/hostapd/hostapd.conf
check_errors
echo "Wrote to /etc/hostapd/hostapd.conf"


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Writing to /etc/default/hostapd..."
a="
DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"
"
sudo sh -c "echo '$a'>/etc/default/hostapd"
sudo chmod 777 /etc/default/hostapd
check_errors
echo "Wrote to /etc/default/hostapd"



echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"




echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

date


end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."
