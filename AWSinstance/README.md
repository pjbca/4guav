# Outline

This script and files configure and set up an Amazon Web Service (AWS) Linux instance for coordinating the UAV communication over 4g modem with a ground control station.


## Target hardware/prerequisites

* AWS account (cloud)
* UAV with Rasbperry Pi Zero W, 4g modem (USB 730L) configured as in this repo

The scripts assume the home on the AWS instance is:
/home/ubuntu

## Installing

Provision an AWS instance.
I used Linux Ubuntu 18.04.
Any version of Linux should work.
Set up for public IP (called AWSIPADRESS in this repo).

Set up the AWS firewall (done through web based interface on AWS, not inside the Linux virtual machine):
* Open port 22 only to incoming/outgoing traffic from anywhere
* Open port 8080 to traffic only from AWSIPADRESS, i.e. its self


Get the script to install and configure the AWS instance:
```
wget https://gitlab.com/pjbca/4guav/raw/Development/AWSinstance/AWSbuild.sh
```

Run script:
```
sudo chmod 777 ~/AWSbuild.sh; 
sudo ~/AWSbuild.sh | tee buildlog.txt
```

Done!

To confirm it works:
* Connect to flight controller over serial (see schematics) and confirm it connects to the mavproxy router by monitoring it's screen (see below).
* Check that it connects to AWS properly (see below).


### AWS firewall

Here are screenshots of how the incoming and outgoing firewall should be configured:
![alt text](https://gitlab.com/pjbca/4guav/raw/master/AWSinstance/AWSinboundrule.png)

![alt text](https://gitlab.com/pjbca/4guav/raw/master/AWSinstance/AWSoutboundrules.png)

### IP address

The AWS instance needs to have a public IP address that other computers can talk to over the internet. See the AWS documentation about that.

In my case, I prefer the use of an AWS "ellastic IP address". This is an IP address associated with my account, so I can assign it to any AWS instance I want, which I find useful so I use the same IP address. See: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html



## How it works:

### Mavlink packets overview

The AWS instance runs mavlink-router as a service using systemd.

Mavlink-router is configured to actively connect to localhost:1234 hence (through SSH tunnel that was esablished by the Pi) to Pi localhost:5678.
This is the "source" of the mavlink traffic for the AWS.

Mavlink-router is also configured to "listen" on localhost:6789.
The ground control station computer will need to ssh into the AWS instance, so the Mission Planner can connect.
The ground control station computer "calls" AWS with this command:
```
ssh -L 7890:localhost:6789 -i "AWSKEY.pem" ubuntu@AWSIPADDRESS
```
So from the ground control station, Mission planner can connect to 127.0.0.1:7890, which is mapped via the ssh tunnel to the AWS localhost:6789. See the wiki for a diagram of this.


### Webcam overview

For the webcam traffic, the Pi establishes a reverse ssh tunnel from AWS localhost:8080 to Pi localhost:80.
The ground control station needs to establish another ssh tunnel for socks proxy web browser using:
```
ssh -D 4444 -i "AWSkey.PEM" ubuntu@AWSIPADDRESS.
```
Then the web browswer on the ground control station can be set to proxy (localhost:4444) which will tunnel to AWS AWSIPADDRESS:8080, which in turn is sent through the Pi-AWS tunnel to Pi port localhost:80.
Regarding webcam traffic for the AWS instance, the only thing the AWS instance needs to do is to have port 8080 open to itself in the AWS firewall configuration page.
I.e. open the port for AWSIPADDRESS:8080. This is done on the AWS web based configuration page; nothing within the Linux virtual machine needs to be configured.

### Configuration details

The script installs a whole bunch of packages using apt-get install.

It downloads mavlink-router source code from git hub, compiles it. (Version 2.0)

mavlink-router is what it says: it passes mavlink packets from one place to another.

It downloads this file (from this repo) for automatically running mavlink-router:
* /etc/main.conf has the parameters for the ports (connects to AWS localhost:1234 which is tunneled already to Pi localhost:6789; also opens a port to listen on localhost:7890 which will be ssh tunneled to from the GCS computer)



## How to test it:

### How to test if autostart worked on boot:

Check status of the service:
```
systemctl status mavlink-router
```

Check logs of the service:
```
journalctl -u mavlink-router
```

Check logs of the service with end updated dyamically:
```
journalctl -u mavlink-router -f
```



### Check all the configuration files:
The following configuration files are created/modified during the build. You can check to see if they were created properly with the correct content:

* /etc/mavlink-router/main.conf
* /home/ubuntu/AWSbuild.sh

## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

 * Thanks to the developers of mavlink-router and Ardupilot.
