#!/bin/bash

# Copyright Peter Burke 8/15/2019

# define functions first



# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

#***********************END OF FUNCTION DEFINITIONS******************************
start_time="$(date -u +%s)"

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install getrssi.sh on Pi and set it up for you."
echo "See README in 4guav gitlab repo for documentation."


echo "Starting..."


time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
time sudo apt-get -y upgrade # 3.5 min

time sudo apt-get install links2 -y; 


cd /home/pi
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/getrssi.sh -O /home/pi/getrssi.sh 
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/start_getrssi.sh -O /home/pi/start_getrssi.sh 
sudo chmod 777 *.sh


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Now make log directory"
if [ ! -d "/home/pi/log" ] 
then
    echo "Directory /home/pi/log does not exist yet. Making it." 
    sudo -u pi mkdir /home/pi/log
    echo "Made /home/pi/log" 
fi

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Now get service"
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/getRSSI.service -O /etc/systemd/system/getrssi.service 
sudo systemctl enable getrssi
sudo systemctl start getrssi

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

