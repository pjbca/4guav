#!/bin/bash

set -e
set -x

# start mavlink router hopefully /etc/mavlink-router/main.conf loads:

touch /home/pi/rssihistory.txt # in case it doesn't exist, create it to avoid problems

#filename=$(TZ='America/Los_Angeles' date +%Y-%m-%d--%H-%m-%SPSTarchive.txt)
TZ='America/Los_Angeles' date
date

#mv ~/rssihistory.txt ~/archive/`TZ='America/Los_Angeles' date +"%d-%m-%Y--%H-%M-%SPSTarchive.txt"`
mv /home/pi/rssihistory.txt /home/pi/log/`TZ='America/Los_Angeles' date +"%d-%m-%Y--%H-%M-%SPSTarchive.txt"`

#today=''date +%Y-%m-%d--%H-%m-%SG''
#filename="$todayGMTarchive.txt"
#echo $filename

#cp ~/rssihistory.txt ~/$filename

#rm ~/rssihistory.txt

echo "starting new file on " >> /home/pi/rssihistory.txt

date >> /home/pi/rssihistory.txt

TZ='America/Los_Angeles' date >> /home/pi/rssihistory.txt 

# This is in service now
# ~/getrssi.sh 

