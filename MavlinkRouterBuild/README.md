# Outline

This installs Mavlink-Router and sets it up to ssh into an AWS as part of a 4g cellular enabled system.
It also connects to the flight controller over serial using Mavlink protocol, assuming a UART connection to the flight controller.

## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* Omnibus F4 Pro V2 flight controller
* UART cable from flight controller to Pi Zero W (see schematics)


It assumes /home/pi is your home directory.

## Installing

Download and unzip the Rasbpian OS image from:

```
https://www.raspberrypi.org/downloads/raspbian/
```
Follow the instructions here to copy the image to an sd card.

```
https://www.raspberrypi.org/documentation/installation/installing-images/README.md
```

Boot Pi with SD.

Config PI using
```
sudo raspi-config
```

* Setup locale (mine is US, US-EN-UTF8)
* Setup keyboard (mine is 101 generic US English no options)
* Setup auto-login
* Enable ssh login
* Set serial options: serial login NO; serial enable YES
* Enable webcam (if you plan to use it)

Result should be in /etc/config.txt that this line appears: “enable_uart=1”

* Set up wifi for your local access point
* reboot Pi

```
sudo reboot
```

Get the script to install and configure the Pi:
```
wget https://gitlab.com/pjbca/4guav/raw/master/MavlinkRouterBuild/MavlinkRouterBuild.sh
```

Run script (takes about an hour to run):
```
sudo chmod 777 ~/MavlinkRouterBuild.sh; 
sudo ~/MavlinkRouterBuild.sh 2>&1 | tee MavlinkRouterBuildlog.txt 
```



Next, get your AWS password key file onto the Pi somehow.
Log into the AWS console for directions on how to get the .pem keyfile.
On the Pi, give it this name:
/home/pi/AWSKEY.pem
You need to set its permission carefully for AWS to accept it:
```
sudo chmod 400 /home/pi/AWSKEY.pem
```

Before you get going, you need to connect manually as sudo to the AWS terminal:
```
sudo ssh -i "/home/pi/AWSKEY.pem" ubuntu@AWSIPADRESS
```
where AWSIPADRESS is the IP address. That way, you can be sure you get into AWS server.
You will be prompted to accept, say yes. This makes the hosts file happy. Otherwise the ssh services will fail.

Reboot Pi:
```
sudo reboot
```

Reboot Pi again:
```
sudo reboot
```

Done!

To confirm it works:
* Connect to flight controller over serial (see schematics) and confirm it connects to the mavproxy router by monitoring it's screen (see below).
* Check that it connects to AWS properly (see below).

## How it works:

The script installs a whole bunch of packages using apt-get install.

It downloads mavlink-router source code from git hub, compiles it. (Version 2.0)

mavlink-router is what it says: it passes mavlink packets from one place to another.

It downloads these files file (from this repo) for automatically running mavlink-router and the ssh:
* /etc/systemd/system/sshtoAWSforterminal.service
* /etc/systemd/system/sshtoAWS.service
* /etc/main.conf has the parameters for the ports (connects to flight controller on UART via /dev/AMA0 and opens Mavlink stream on TCP localhost:5678)

In order to pass Mavlink packets to a cloud server securely and behind a NAT (which we assume here to be an Amazon Web Services AWS instance although others are possible), we need an ssh.

We use a reverse tunnel, and use auto-ssh so that the Pi will "call back" to AWS if the ssh link goes down.

The tunnel maps localhost:1234 on the AWS to localhost:5678 on the Pi. This tunnel will carry the Mavlink traffic: 
mavlink-router on AWS will be the client and mavlink-router on Pi will be the server.

You set the IP address of the AWS instance when the install queries you on the command line.

It can be manually changed later by editing the sshtoAWS.service and sshtoAWSforterminal.service file in /etc/systemd/system.

***

If you want to ssh into the onboard computer Pi, you can. We establish a third reverse ssh tunnel for this purpose only.


The tunnel maps localhost:6000 on the AWS to localhost:22 on the Pi. 
You can used this tunnel to connect to the Pi from an AWS terminal using this command:

```
ssh -p 6000 pi@localhost
```

You set the IP address of the AWS instance when the install queries you on the command line.

It can be manually changed later by editing the sshtoAWS_forterminal.service file.



## How to test it:

### How to check the status of mavlink-router service:
Check status of the service:
```
systemctl status mavlink-router
```

Check logs of the service:
```
journalctl -u mavlink-router
```

Check logs of the service with end updated dyamically:
```
journalctl -u mavlink-router -f
```
### How to check the status of sshtoAWS service:
Check status of the service:
```
systemctl status sshtoAWS
```

Check logs of the service:
```
journalctl -u sshtoAWS
```

Check logs of the service with end updated dyamically:
```
journalctl -u sshtoAWS -f
```

### How to check the status of sshtoAWSforterminal service:
Check status of the service:
```
systemctl status sshtoAWSforterminal
```

Check logs of the service:
```
journalctl -u sshtoAWSforterminal
```

Check logs of the service with end updated dyamically:
```
journalctl -u sshtoAWSforterminal -f
```


### How to test if autostart worked on boot:


Also, there should be two ssh tunnels present on the AWS server. You can check it on the AWS server by logging into the AWS server and running this command, for example:
```
sudo tcptrack -i eth0 port 22
```


### Manual testing:

Test connection manually to UAV with mavproxy:
```
sudo -s mavproxy.py --master=/dev/ttyAMA0 --baudrate 57600
```
Note: you need AMA0, not S0.

Only baud of 57600 works for me with Omnibus F4 Pro V2/Chibios.

To manually invoke mavlink-router:

```
cd ~/mavlink-router
/.mavlink-routerd # (will use main.conf file)
```

### Check all the configuration files:
The following configuration files are created/modified during the build. You can check to see if they were created properly with the correct content:

* /boot/config.txt
* /home/pi/MavlinkRouterBuild.sh
* /etc/systemd/system/sshtoAWSforterminal.service
* /etc/systemd/system/sshtoAWS.service
* /etc/mavlink-router/main.conf has the parameters for the ports (connects to flight controller on UART via /dev/AMA0 and opens Mavlink stream on TCP localhost:5678)
* 

## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

 * Thanks to the developers of mavlink-router and Ardupilot.
