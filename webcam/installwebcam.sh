#!/bin/bash

# Copyright Peter Burke 12/4/2018

# define functions first


function fxyz {
    echo "doing function fxyz"
}



function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
    time sudo apt-get -y upgrade # 3.5 min

    time sudo apt-get install git -y; 
    # time sudo apt-get install screen -y; 
    time sudo apt-get install autossh -y; 
    time sudo apt-get install php7.4 -y;

    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}


function installpicam {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing picam..."

    start_time_installstuff="$(date -u +%s)"

    cd /home/pi

    git clone https://github.com/silvanmelchior/RPi_Cam_Web_Interface.git

    cd RPi_Cam_Web_Interface

    sudo chmod 777 install.sh; 

    # sudo ~/RPi_Cam_Web_Interface/install.sh
    # User does this manually later so he/she can answer all the prompts it asks!!!


    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

function downloadServiceFilesforAWSssh {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Now download the service files for ssh to AWS"

    wget https://gitlab.com/pjbca/4guav/raw/master/webcam/sshtoAWSforWebCam.service -O /etc/systemd/system/sshtoAWSforWebCam.service
    # /etc/systemd/system is directory to put files into

    echo "Did download the service files for ssh to AWS"

    
}


function inputAWSIPaddressAndInputIntoServiceFile {
    read -p "To begin with the installation type in the EXACT IP address of the AWS instance for the ssh tunnel: " awsip

    echo "You entered:"
    echo $awsip

    read -p "If this is correct, enter "yes": " out

    if ! [ "$out" = "yes" ]
    then
        echo "You did not type in 'yes'. Exiting....AWS IP address not in yet. Please check documentation."
        exit 1
    fi
    
    
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

    echo "Editing service files to put IP address in that you entered above."

    # awsip was entered above
    # sed -i "s/AWSIPADDRESS/$awsip/g" servicefile
    sed -i "s/AWSIPADDRESS/$awsip/g" /etc/systemd/system/sshtoAWSforWebCam.service

    # new we also have to add it to known hosts 
    # ssh-keyscan /$awsip >> ~/.ssh/known_hosts # this doesn't work if known_hosts doesn't exist. To do: Fix this.
    # For now, manually ssh in to AWS from pi after install to make it work.

    
}





# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

#***********************END OF FUNCTION DEFINITIONS******************************



#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "Starting..."

date

start_time="$(date -u +%s)"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install webccam on Pi and set it up for you."
echo "See README in 4guav gitlab repo for documentation."
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Starting..."

installstuff

installpicam

#inputAWSIPaddress
downloadServiceFilesforAWSssh
inputAWSIPaddressAndInputIntoServiceFile

#enable the ssh to AWS service and start it
sudo systemctl enable sshtoAWSforWebCam
sudo systemctl start sshtoAWSforWebCam

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

